import NotesController from '../../src/notes/notes-controller';

test('hello', async () => {
    const notesDao = {
        save: jest.fn()
    }
    const res = {
        json: jest.fn()
    }
    const next = jest.fn();

    const notesController = new NotesController(notesDao)


    await notesController.createNote({body: {content: 'test'}}, res, next)

    expect(next.mock.calls.length).toBe(0);

    expect(res.json.mock.calls.length).toBe(1);
    expect(notesDao.save.mock.calls.length).toBe(1);
    expect(notesDao.save.mock.calls[0][0]).toStrictEqual({content: 'test'});
});
