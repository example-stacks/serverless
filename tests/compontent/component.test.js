import "../setEnvVars"
import request from "supertest";
import routes from "../../src/routes";

describe("Test API", () => {
    test("It should response the GET method under root with 404", async () => {
        const response = await request(routes).get("/")

        expect(response.statusCode).toBe(404);
    });


    test("It should reject saving note on validation", async () => {
        const response = await request(routes).post("/api/notes")

        expect(response.statusCode).toBe(400);
    });

    test("It should save note", async () => {
        const response = await request(routes).post("/api/notes").send({content: "test"})

        expect(response.statusCode).toBe(200);
    });
});
