import AWS from 'aws-sdk';
import stack from '../stack';

const dynamoDb = stack.isOffline === 'true' ? new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000',
    accessKeyId: 'DEFAULT_ACCESS_KEY',
    secretAccessKey: 'DEFAULT_SECRET'
}) : new AWS.DynamoDB.DocumentClient();

export {
    dynamoDb
};
