import querystring from 'querystring'

const encodeKey = (key) => {
    return Buffer.from(JSON.stringify({
        key
    })).toString('base64')
};

const decodeKey = (encoded) => {
    return JSON.parse(Buffer.from(querystring.unescape(encoded), 'base64').toString()).key;
};

export {
    encodeKey,
    decodeKey
}
