import log from "lambda-log";
import {ValidationError} from 'express-validation';

const CacheMiddleware = (req, res, next) => {
    res.set('CacheControl', 'no-cache, no-store, must-revalidate');
    res.set('Expires', '2018-08-03T17:36:38Z');

    next();
};

const LoggingMiddleware = (req, res, next) => {
    log.options.dynamicMeta = () => {
        return {
            timestamp: new Date().toISOString()
        };
    };
    next();
};

const ErrorResolver = function (err, req, res, next) {
    if (err instanceof ValidationError) {
        res.status(400).json(err);
    } else {
        log.error(err);
        res.status(500).send('Something broke!');
    }
};

export {CacheMiddleware, LoggingMiddleware, ErrorResolver};
