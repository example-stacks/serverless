import express from 'express';

const {validate, ValidationError, Joi} = require('express-validation')


function NotesRouter(notesController) {
    const routes = express.Router({mergeParams: true});

    routes.get('', notesController.listNotes);

    routes.post('', validate({
        body: Joi.object().keys({
            content: Joi.string().required().max(1000),
        })
    }), notesController.createNote);

    return routes;
}

export default NotesRouter;
