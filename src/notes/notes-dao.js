import {v4 as uuidv4} from 'uuid';
import {decodeKey, encodeKey} from "../plugins/dynamo-key";

function NotesDao(stack, dynamoDb) {

    const save = async (note) => {
        note.id = uuidv4();
        note.createdAt = new Date().getTime();
        let params = {
            TableName: stack.notesTable,
            Item: note
        };
        await dynamoDb.put(params).promise()
        return note;
    };

    const load = async (lastEvaluatedKey) => {
        const params = {
            TableName: stack.notesTable,
            Limit: 2,
            ScanIndexForward: false
        };
        if (lastEvaluatedKey) {
            params['ExclusiveStartKey'] = decodeKey(lastEvaluatedKey);
        }
        let {Items, LastEvaluatedKey} = await dynamoDb.scan(params).promise()
        return {
            items: Items,
            lastEvaluatedKey: LastEvaluatedKey ? encodeKey(LastEvaluatedKey) : null
        }
    };

    return {
        save,
        load
    }
}

export default NotesDao
