import NotesController from "./notes-controller";
import NotesRouter from "./notes-router";
import NotesDao from "./notes-dao";

import stack from '../stack';
import {dynamoDb} from '../aws';

const notesDao = new NotesDao(stack, dynamoDb);
const notesController = new NotesController(notesDao);
const notesRouter = new NotesRouter(notesController);

export {
    notesRouter
}
