import log from 'lambda-log';

function NotesController(notesDao) {

    const listNotes = async (req, res, next) => {
        try {
            log.info('Listing notes');
            res.json(await notesDao.load(req.query.lastEvaluatedKey));
        } catch (e) {
            log.error('Failed to list notes', e);
            next(e);
        }
    };

    const createNote = async (req, res, next) => {
        try {
            log.info('Creating note');
            res.json(await notesDao.save(req.body));
        } catch (e) {
            log.error(`Failed to create note ${e}`);
            next(e);
        }
    };

    return {
        listNotes,
        createNote
    }
}

export default NotesController
