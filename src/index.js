import routes from './routes';
import serverless from 'serverless-http';

export const handler = async (event, context) => {
    return await serverless(routes, {
        request: (req, event) => {
            req.context = event.requestContext.authorizer;
        }
    })(event, context);
};
