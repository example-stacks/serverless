const stack = {
    "isOffline": process.env.IS_OFFLINE,
    "notesTable": process.env.NOTES_TABLE,
};

export default stack;
