import express from 'express';

import {CacheMiddleware, ErrorResolver, LoggingMiddleware} from "./plugins/middleware";
import {notesRouter} from "./notes";

const routes = express();

routes.use(CacheMiddleware);
routes.use(LoggingMiddleware);
routes.use(express.json());

routes.use('/api/notes', notesRouter);

routes.use(ErrorResolver);

export default routes;
